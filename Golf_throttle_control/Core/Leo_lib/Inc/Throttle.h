/*
 * Throttle.h
 *
 *  Created on: Sep 21, 2021
 *      Author: btunc
 */

#ifndef INC_THROTTLE_H_
#define INC_THROTTLE_H_

#include "stdint.h"

#define OPAMP_GAIN 3
#define AI2_MIN 0.3632f
#define AI2_MAX 2.16f

#define AI1_MIN 0.7225f
#define AI1_MAX 4.32f


typedef struct{
	uint16_t adc1;
	uint16_t adc2;
	float 	 Ai1;
	float	 Ai2;
	uint16_t dac1;
	uint16_t dac2;
	float 	 Ao1;
	float	 Ao2;

}typedef_throttle;
typedef struct{
	float	 set_throttle;
	float 	 pedal_pos;
	uint8_t  autonom_enable;
}typedef_CANmsgs;

typedef struct{
	uint8_t timeout;
	uint8_t pedal_conn;

}typedef_err;

typedef struct{
	typedef_throttle throttle;
	typedef_err errors;
	typedef_CANmsgs canmsgs;
}typedef_Acc;


void set_throttle(float throttle);
float throttle_map(float throttle);
float get_accpedal_pos(float A1,float A2);
float adctovoltage(uint16_t adc);
uint16_t voltagetodacval(float A1);


#endif /* INC_THROTTLE_H_ */
