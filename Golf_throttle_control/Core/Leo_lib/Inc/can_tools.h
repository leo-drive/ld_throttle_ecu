#ifndef __CAN_TOOLS_H
#define __CAN_TOOLS_H

#include "main.h"
#include "stdint.h"
#include "cmsis_os.h"


#define true 	1
#define false 0

typedef struct {
	CAN_HandleTypeDef *hcan;
	CAN_TxHeaderTypeDef pHeader;
	uint8_t data[8];
	uint8_t Tr;

} typedef_CanMsg;

typedef struct {
	uint8_t Act_Notif;
	uint8_t Init;
	uint8_t Start;
	uint8_t ConfigFilter;
	struct {
		uint8_t noProperId;
		uint8_t noValidDLC;
	} Send;
	uint8_t Receive;
} CAN_Error_Struct;

extern CAN_HandleTypeDef hcan1;


void CAN_Start(void);
void CAN1_GetMessage(void);
void CAN_FilterConfig(void);
void CAN_FilterDoubleID(CAN_HandleTypeDef *hcan, uint32_t FilterBank,
		uint32_t ID1, uint32_t ID2);
void CAN_SendMessage(CAN_HandleTypeDef *hcan, uint32_t ID, uint32_t dlc,
		uint8_t *data);

#endif



