/*
 * can_msgs.h
 *
 *  Created on: Sep 22, 2021
 *      Author: btunc
 */

#ifndef INC_CAN_MSGS_H_
#define INC_CAN_MSGS_H_

#define CAN_COMMAND_ID 0x474
#define COMMAND_DLC 3
#define CAN_FEEDBACK_ID 0x475
#define FEEDBACK_DLC 3

#include "stdint.h"
#include "Throttle.h"





void unpack_command(uint8_t* data, typedef_CANmsgs *msgs);
void pack_feedback(uint8_t* data, const typedef_Acc *msgs);

#endif /* INC_CAN_MSGS_H_ */
