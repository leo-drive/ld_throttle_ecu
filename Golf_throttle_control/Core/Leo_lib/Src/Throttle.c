/*
 * Throttle.c
 *
 *  Created on: Sep 21, 2021
 *      Author: btunc
 */
#include "main.h"
#include "Throttle.h"
extern typedef_Acc Gas_var;
extern DAC_HandleTypeDef hdac;
void set_throttle(float throttle)//%
{
	Gas_var.throttle.dac1 = voltagetodacval(throttle_map(throttle));
	Gas_var.throttle.dac2 = Gas_var.throttle.dac1 / 2;
	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, Gas_var.throttle.dac1);
	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, Gas_var.throttle.dac2);
}
//returns A1 voltage
float throttle_map(float throttle){//%
	float A1 = 0, A1_offset = 0;
	A1_offset = AI1_MIN;
	A1 = (AI1_MAX - AI1_MIN) * 0.01f * throttle;
	A1 += A1_offset;

	return A1;
}

float get_accpedal_pos(float A1,float A2){
	float pedal_pos = 0;
	//if(A1 - 2 * A2 < 0.05f)
	//{
		float ort = 0;
		ort = (A1 + 2 * A2) * 0.5f;
		pedal_pos = (ort - AI1_MIN) / (AI1_MAX - AI1_MIN) * 100.0f;
		pedal_pos = pedal_pos > 100 ? 100 : pedal_pos;
	//}
	//else{
		//err
	//}

	return pedal_pos;
}

float adctovoltage(uint16_t adc){
	float vol = 0;
	vol = (float)adc * 3.3f / 4095.0f;
	return vol;
}

uint16_t voltagetodacval(float A1)
{
	uint16_t DAC_val = 0;

	DAC_val = A1 * 4095 / 3.3f / OPAMP_GAIN;

	return DAC_val;

}


