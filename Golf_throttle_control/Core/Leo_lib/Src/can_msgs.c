/*
 * can_msgs.c
 *
 *  Created on: Sep 22, 2021
 *      Author: btunc
 */
#include "can_msgs.h"
#include "main.h"
#include "Throttle.h"
#include "stdint.h"
#include "can_tools.h"
#include "string.h"



void unpack_command(uint8_t* data, typedef_CANmsgs *msgs){

	uint16_t set_accpos = 0;

	memcpy(&set_accpos, data, 2);

	msgs->set_throttle = (float)set_accpos * 0.01f;
	msgs->autonom_enable = data[2] & 0x01;
}

void pack_feedback(uint8_t* data, const typedef_Acc *msgs){
	uint16_t pedal_pos = 0;

	pedal_pos = (uint16_t)(msgs->canmsgs.pedal_pos * 100.0f);
	memcpy(data, &pedal_pos,2);
	data[2] = (msgs->errors.timeout & 0x01) | ((msgs->errors.timeout & 0x01) << 1);

}


