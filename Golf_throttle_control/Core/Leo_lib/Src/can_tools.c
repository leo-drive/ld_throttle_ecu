#include "can_tools.h"
#include "can_msgs.h"
#include "can_msgs.h"
#define CAN_TX_BUFFER_SIZE 20

typedef_CanMsg Canbuffer[CAN_TX_BUFFER_SIZE];
CAN_TxHeaderTypeDef pHeader;
uint8_t indx = 0;

extern typedef_Acc Gas_var;

struct {
	CAN_Error_Struct Can1;
	CAN_Error_Struct Can2;
} myErrorFlag;



void CAN_FilterConfig(void) {

	/* CAN 1 Filters */
	CAN_FilterDoubleID(&hcan1, 0, CAN_COMMAND_ID, 0);


	/* CAN 2 Filters */
	//CAN_FilterDoubleID(&hcan2, 14, BCU_TO_AUTO_ID, BCU_TO_FDBCK_ID);


}

void CAN_Start(void) {

	/* Start CAN 1 */
	if (HAL_CAN_Start(&hcan1) != HAL_OK) {
		myErrorFlag.Can1.Start = true;

	}

	/* Enable CAN 1 RX Interrupts */
	if (HAL_CAN_ActivateNotification(&hcan1,
	CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) {
		myErrorFlag.Can1.Act_Notif = true;

	}


}



void CAN_FilterDoubleID(CAN_HandleTypeDef *hcan, uint32_t FilterBank,
		uint32_t ID1, uint32_t ID2) {

	CAN_FilterTypeDef sFilterConfig;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDLIST;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;

	if (ID1 == 0) {
		;
	} else if (IS_CAN_STDID(ID1)) {
		sFilterConfig.FilterIdHigh = (uint32_t) ID1 << 5;
		sFilterConfig.FilterIdLow = (uint32_t) 0;
	} else if (IS_CAN_EXTID(ID1)) {
		sFilterConfig.FilterIdHigh = (uint32_t) ID1 >> 13;
		sFilterConfig.FilterIdLow = (uint32_t) (((ID1 & (uint32_t) 0x1FFF) << 3)
				| 4);
	}

	if (ID2 == 0) {
		;
	} else if (IS_CAN_STDID(ID2)) {
		sFilterConfig.FilterMaskIdHigh = (uint32_t) ID2 << 5;
		sFilterConfig.FilterMaskIdLow = (uint32_t) 0;
	} else if (IS_CAN_EXTID(ID2)) {
		sFilterConfig.FilterMaskIdHigh = (uint32_t) ID2 >> 13;
		sFilterConfig.FilterMaskIdLow = (uint32_t) (((ID2 & (uint32_t) 0x1FFF)
				<< 3) | 4);
	}

	sFilterConfig.FilterFIFOAssignment = CAN_FilterFIFO0;
	sFilterConfig.FilterActivation = CAN_FILTER_ENABLE;
	sFilterConfig.FilterBank = FilterBank;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(hcan, &sFilterConfig) != HAL_OK) {
		myErrorFlag.Can1.ConfigFilter = true;
		Error_Handler();
	}
}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
	/* Prevent unused argument(s) compilation warning */
	UNUSED(hcan);
	uint32_t p1TxMailbox;
	int i;
	for (i = 0; i < CAN_TX_BUFFER_SIZE; i++) {
		if (Canbuffer[i].Tr == 1) {
			if (Canbuffer[i].hcan == hcan) {
				if (HAL_CAN_AddTxMessage(Canbuffer[i].hcan,
						&Canbuffer[i].pHeader, Canbuffer[i].data, &p1TxMailbox)
						== HAL_OK) {
					Canbuffer[i].Tr = 0;
					break;
				}
			}
		}

	}

}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
	/* Prevent unused argument(s) compilation warning */
	UNUSED(hcan);
	uint32_t p1TxMailbox;
	int i;
	for (i = 0; i < CAN_TX_BUFFER_SIZE; i++) {
		if (Canbuffer[i].Tr == 1) {
			if (Canbuffer[i].hcan == hcan) {
				if (HAL_CAN_AddTxMessage(Canbuffer[i].hcan,
						&Canbuffer[i].pHeader, Canbuffer[i].data, &p1TxMailbox)
						== HAL_OK) {
					Canbuffer[i].Tr = 0;
					break;
				}
			}
		}

	}

}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
	/* Prevent unused argument(s) compilation warning */
	UNUSED(hcan);
	uint32_t p1TxMailbox;
	int i;
	for (i = 0; i < CAN_TX_BUFFER_SIZE; i++) {
		if (Canbuffer[i].Tr == 1) {
			if (Canbuffer[i].hcan == hcan) {
				if (HAL_CAN_AddTxMessage(Canbuffer[i].hcan,
						&Canbuffer[i].pHeader, Canbuffer[i].data, &p1TxMailbox)
						== HAL_OK) {
					Canbuffer[i].Tr = 0;
					break;
				}
			}
		}

	}

}

void CAN_SendMessage(CAN_HandleTypeDef *hcan, uint32_t ID, uint32_t DLC,
		uint8_t *data) {
	uint32_t pTxMailbox;

	if (IS_CAN_STDID(ID)) {
		pHeader.IDE = CAN_ID_STD;
		pHeader.StdId = ID;
	} else if (IS_CAN_EXTID(ID)) {
		pHeader.IDE = CAN_ID_EXT;
		pHeader.ExtId = ID;
	} else {
		if (hcan == &hcan1) {
			myErrorFlag.Can1.Send.noProperId = true;
		}
		//	else if (hcan == &hcan2) {
		//	myErrorFlag.Can2.Send.noProperId = true;
		//}
	}

	if (DLC <= 8) {
		pHeader.DLC = DLC;
	} else {
		if (hcan == &hcan1) {
			myErrorFlag.Can1.Send.noValidDLC = true;
		}
		//	else if (hcan == &hcan2) {
		//	myErrorFlag.Can2.Send.noValidDLC = true;
		//}
	}

	if (HAL_CAN_GetTxMailboxesFreeLevel(hcan) != 0) // check mailboxes
			{
		HAL_CAN_AddTxMessage(hcan, &pHeader, data, &pTxMailbox);
	} else //store data
	{
		if (Canbuffer[indx].Tr == 1)
			indx++;
		if (indx >= (CAN_TX_BUFFER_SIZE - 1))
			indx = 0;

		Canbuffer[indx].hcan = hcan;
		Canbuffer[indx].pHeader = pHeader;
		for (int i = 0; i < 8; i++) {
			Canbuffer[indx].data[i] = *data++;
		}
		Canbuffer[indx].Tr = 1;
	}
}

void CAN1_GetMessage(void) {
	CAN_RxHeaderTypeDef myRxHeader;
	uint8_t CAN_Rx_Buffer[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	HAL_CAN_GetRxMessage(&hcan1, CAN_FilterFIFO0, &myRxHeader, CAN_Rx_Buffer);

	if (myRxHeader.IDE == CAN_ID_STD) {

		if(myRxHeader.StdId == CAN_COMMAND_ID )
		{
			unpack_command(CAN_Rx_Buffer, &Gas_var.canmsgs);
		}
	}

}








